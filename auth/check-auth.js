const jwt = require("jsonwebtoken");
module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    const decoded = jwt.verify(token, "decipher");
    req.userData = decoded;
  } catch (error) {
    return res.json({ message: "auth failed" });
  }
  next();
};
