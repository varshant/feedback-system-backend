var mongoose = require("mongoose");
require("mongoose-type-email");
const adminSchema = mongoose.Schema({
  name: { type: String, required: true },
  email: { type: mongoose.SchemaTypes.Email, required: true },
  approverId: { type: String, required: true }
});

module.exports = mongoose.model("admin", adminSchema);
