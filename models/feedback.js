var mongoose = require("mongoose");
require("mongoose-type-email");
const feedbackSchema = mongoose.Schema({
  interviewerId: { type: String, required: true },
  applicantName: { type: String, required: true },
  date: { type: String, required: true },
  workExperience: {
    years: { type: Number, required: true },
    months: { type: Number, required: true }
  },
  applyingFor: { type: String, required: true },

  skills: {
    type: [
      {
        name: { type: String, required: true },
        rating: { type: Number, required: true }
      }
    ],
    required: true
  },
  comments: { type: String, required: true },
  status: { type: String, required: true }
});

module.exports = mongoose.model("feedback", feedbackSchema);
