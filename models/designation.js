var mongoose = require("mongoose");

const designationSchema = mongoose.Schema({
  name: { type: String, required: true }
});

module.exports = mongoose.model("designation", designationSchema);
