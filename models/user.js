var mongoose = require("mongoose");
require("mongoose-type-email");
const userSchema = mongoose.Schema({
  name: { type: String, required: true },
  email: { type: mongoose.SchemaTypes.Email, required: true },
  password: { type: String, required: true },
  status: { type: String, required: true },
  approverId: { type: String, required: true },
  requestType: { type: String, required: true }
});

module.exports = mongoose.model("user", userSchema);
