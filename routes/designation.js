const express = require("express");
const router = express.Router();
const Designation = require("../utils/designation");
const checkAuth = require("../auth/check-auth");
router.get("/", checkAuth, (req, res, next) => {
  Designation.fetch({}, (err, doc) => {
    if (err) {
      console.log(err);
      res.status(500).json({
        error: err
      });
    }
    res.status(200).json({ message: "success", doc: doc });
  });
});
router.post("/", checkAuth, (req, res, next) => {
  Designation.check(req.body, (err, doc) => {
    if (err) {
      res.status(500).json({
        error: err
      });
    } else if (doc.length) {
      res.json({ message: "exists" });
    } else {
      Designation.add(req.body, (err, result) => {
        if (err) {
          console.log(err);
          res.status(500).json({
            error: err
          });
        }
        res.status(200).json({ doc: result, message: "added" });
      });
    }
  });
});
router.delete("/:id", checkAuth, (req, res, next) => {
  Designation.delete({ _id: req.params.id }, (err, doc) => {
    if (err) {
      throw err;
    }
    res.json({ message: "deleted", user: doc });
  });
});
module.exports = router;
