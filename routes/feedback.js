const express = require("express");
const router = express.Router();
const Feedback = require("../utils/feedback");
const fs = require("file-system");
var multer = require("multer");
const checkAuth = require("../auth/check-auth");
const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "resumes/");
  },
  filename: function(req, file, cb) {
    console.log(req.body.interviewID);
    cb(null, req.body.interviewID);
  }
});
const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === "application/msword" ||
    file.mimetype === "application/pdf" ||
    file.mimetype ===
      "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};
var upload = multer({
  storage: storage,
  limits: { fileSize: 1024 * 1024 * 3 },
  fileFilter: fileFilter
});

router.post("/uploads", checkAuth, upload.single("resume"), (req, res) => {
  console.log(req.file);
  res.json({ message: "upload successful" });
  //   Feedback.add(req.body, (err, doc) => {
  //     if (err) {
  //       console.log(err);
  //       res.status(500).json({
  //         error: err
  //       });
  //     } else {
  //       console.log(doc, "feddback added");
  //       res.json({ doc: doc, message: "successful" });
  //     }
  //   });
});
router.get("/resume/:id", (req, res) => {
  let path = "resumes/" + req.params.id;
  var data = fs.readFileSync(path);
  res.contentType("application/pdf");

  res.send(data);
});

router.post("/", checkAuth, (req, res) => {
  Feedback.add(req.body, (err, doc) => {
    if (err) {
      res.status(500).json({
        error: err
      });
    } else {
      console.log(doc, "feddback added");
      res.json({ doc: doc, message: "feedback added" });
    }
  });
});

router.get("/:id", checkAuth, (req, res, next) => {
  Feedback.fetch({ interviewerId: req.params.id }, (err, doc) => {
    if (err) {
      res.status(500).json({
        error: err
      });
    }
    res.status(200).json(doc);
  });
});

router.delete("/:_id", checkAuth, (req, res, next) => {
  Feedback.deleteFeedbacks({ interviewerId: req.params._id }, (err, doc) => {
    if (err) {
      throw err;
    }
    res.json({ message: "deleted", user: doc });
  });
});
router.delete("/feedback/:_id", checkAuth, (req, res, next) => {
  Feedback.deleteFeedback({ _id: req.params._id }, (err, doc) => {
    if (err) {
      throw err;
    }
    res.json({ message: "deleted", user: doc });
  });
});
router.get("/feedback/:id", checkAuth, (req, res, next) => {
  Feedback.fetch({ _id: req.params.id }, (err, doc) => {
    if (err) {
      res.status(500).json({
        error: err
      });
    }
    res.status(200).json({ doc: doc[0], message: "successful" });
  });
});
router.put("/:id", checkAuth, (req, res) => {
  Feedback.edit({ _id: req.params.id }, req.body, { new: true }, (err, doc) => {
    if (err) {
      throw err;
    }
    res.json({ message: "successful", doc: doc });
  });
});

module.exports = router;
