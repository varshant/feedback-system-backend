const Feedback = require("../models/feedback");
module.exports.add = function(query, callback) {
  Feedback.create(query, callback);
};
module.exports.fetch = function(query, callback) {
  Feedback.find(query, callback);
};
module.exports.deleteFeedbacks = function(query, callback) {
  Feedback.deleteMany(query, callback);
};
module.exports.deleteFeedback = function(query, callback) {
  Feedback.remove(query, callback);
};
module.exports.edit = function(query, update, options, callback) {
  Feedback.findOneAndUpdate(query, update, options, callback);
};
