const Skill = require("../models/skill");

module.exports.fetch = function(query, callback) {
  Skill.find(query, callback);
};
module.exports.add = function(query, callback) {
  Skill.create(query, callback);
};

module.exports.delete = function(query, callback) {
  Skill.remove(query, callback);
};
module.exports.check = function(query, callback) {
  Skill.find(query, callback);
};
